# kotch'up, projet informatique, DUT Stid S1 

Application web pour la gestion des absences, devoirs et stages des étudiants.

**Ce projet est désormais définitivement archivé, mais deployable avec Docker (voir [docker-compose.yml](docker-compose.yml))**

# Collaborateurs
Florian Anceau, [Gaëtan Doré](https://github.com/Hjelad), Nolwenn Lannuel.
