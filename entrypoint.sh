#!/bin/bash

# Start the PHP-FPM daemon, the MySQL server, then the Nginx daemon.
service php7.2-fpm start
service mysql start
nginx