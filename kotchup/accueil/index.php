<?php
  /* definit la profondeur pour simplifier les include */
  $profondeur = "../";
  include($profondeur . "assets/php/hasConnected.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php
      include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h1>Bienvenue sur kotch'up, le site de coaching d'étudiants !</h1>
          <p><b>À toi de gérer tes absences, tes devoirs, tes candidatures de stages avec l'aide des alertes.</b></p>
          <p>Télécharge l'application Excel pour gérer tes notes. <a href="/assets/xlsm/mesnotes.xlsm"><span class="glyphicon glyphicon-save"></span></a></p>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
