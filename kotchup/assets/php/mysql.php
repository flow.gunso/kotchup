<?php
  require_once('defines.php');

  try {
    $connexion = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
  } catch (Exception $e) {
    echo '<p>Impossible de se connecter à la base de données: ' . $e->getMessage() . '<p>';
  }
?>
