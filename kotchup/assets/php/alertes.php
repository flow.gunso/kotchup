<div class="col-md-4 text-right">
	<div id="alerte">
  <?php
  if ( isset($_SESSION["isConnected"]) ) {
    echo '<h1>Tes alertes</h1>';
    $id = $_SESSION['idUtilisateur'];
    include("mysql.php");
    $sql = ' select * from devoir, module where devoir.idModule=module.idModule and devoir.idUtilisateur = '.$id ;
    $requete = $connexion -> query($sql) ; 
    echo '<h3> Tes devoirs </h3>' ; 
    echo '<ul>';
	$nb = 0;

    /* afficher les devoirs prévus dans les prochains jours */	
    while ($ligne = $requete -> fetch() ){
      $nbjours = round((strtotime($ligne["dateDevoir"]) - time())/(60*60*24));
      
      if ($nbjours>=0 && $nbjours<7) {
        echo '<li>'.$ligne["labelModule"].' '.$ligne["infoSupp"].' '.date("d/m/Y",strtotime($ligne["dateDevoir"]))	.' Il te reste '.$nbjours.' jour(s) pour réviser !</li>';
        $nb = $nb + 1;
      }
	}
    if ($nb == 0) { echo '<li> Aucun devoir prévu dans les 7 prochains jours, reposes-toi bien mais ça ne va pas durer ! </li>' ;}   	
    echo '</ul></br><h3>Tes candidatures de stages</h3> <ul>';
    /* regarder si l'etudiant a déjà un stage accepté */
    $acc = false;
    $sql3 = 'select * from stageent where idUtilisateur = '.$id.' and reponse =\'Acceptée\'';
    $requete3 = $connexion -> query($sql3);
    while ($ligne3 = $requete3 -> fetch()){
      $acc =true; /* renvoie true si un stage a déjà été accepter */
    }

    if ($acc == false){
      $sql2 = 'select * from stageent where idUtilisateur = '.$id.' and reponse =\'En cours\' order by date_dem'; /*recherche les candidatures en cours de l'étudiant */
      $requete2 = $connexion -> query($sql2); 
      while($ligne2 = $requete2 -> fetch()){
        $dif = round ((time() - strtotime($ligne2["date_dem"]))/(60*60*24)-1);  /* calcule l'écart en jour entre la date actuel et la date de la candidature de stage */
        if ($dif>60) { 
        
          $rel = false;
          $sql5 = 'select * from relance where idStage ='.$ligne2["idStage"];
          $requete5 = $connexion -> query($sql5);
          
          while ($ligne5= $requete5 -> fetch ()) { /* recherche si une candidature sans réponse a déjà eu une relance */
            $rel = true;
          }
          
          if ($rel == true){
            $sql6 = 'SELECT MAX( date_rel ) as date_max  FROM relance WHERE idStage ='.$ligne2["idStage"]; /* recherche la dernière relance faite pour une candidature */
            $requete6 = $connexion -> query($sql6);
            $ligne6 = $requete6 -> fetch() ;
            $dif2 = round((time() - strtotime($ligne6["date_max"]))/(60*60*24)-1); /* calcule l'ecart en jour entre la dernière relance et le jour actuel */
            if ($dif2 > 10) {
              echo '<li>'.$ligne2["nomOrg"].' '.$ligne2["lieu"].' '.$ligne2["libelle_mis"].' '.$ligne2["reponse"].' <a href="/stages/changer/?action=relancer&id='.$ligne2["idStage"].'"> RELANCE </a></li>';
            }
          } 
          else {
            echo '<li>'.$ligne2["nomOrg"].' '.$ligne2["lieu"].' '.$ligne2["libelle_mis"].' '.$ligne2["reponse"].' <a href="/stages/changer/?action=relancer&id='.$ligne2["idStage"].'"> RELANCE </a></li>';
          }
        }	
      }
      
      $sql4 = 'select count(*) from stageent where idUtilisateur ='.$id ;
      $requete4 = $connexion -> query($sql4);
      $ligne4 = $requete4 -> fetch() ;
      if ($ligne["count(*)"] < 10 && date("n")>=1 &&date("n")<=6){ /* compte le nombre de candidature et situe le mois de l'année */
        echo '<li>Tu devrais te presser pour faire tes candidatures de stages !</li>'; 
      } else {  
        echo '<li> Continue tes recherches de stages ! </li>';
      }


    }
    else {
      $sql2 = 'select * from stageent where idUtilisateur = '.$id.' and reponse =\'En cours\' order by date_dem';
      $requete2 = $connexion -> query($sql2);
      if ($requete2 -> rowCount() > 0 ) {
        while($ligne2= $requete2 -> fetch () ) {
          echo ' <li> Tu devrais prévenir l\'(es) organisation(s) suivante(s) pour la(les) prévenir que tu as déjà une candidature acceptée :  ' .$ligne2["nomOrg"].' '.$ligne2["lieu"].'! </li>';
        }
      } else echo '<li> Félicitations pour avoir trouvé un stage !</li>';
      echo '</br>' ;
      }
      $sql7 = 'select * from absence where idUtilisateur ='.$id; 
      $requete7 = $connexion -> query($sql7) ; 
      echo '</ul></br><h3>Tes absences</h3><ul>';
      while ($ligne7 = $requete7 -> fetch() ) {
        $ligne7["dateFin"] = $ligne7["dateFin"]. ' 8:00:00';
        if(is_null($ligne7["justificatif"])== true){ 
          $temps_abs = round((time() - strtotime($ligne7["dateFin"]))/(60*60)-1);
          if($temps_abs<48 && $temps_abs>0){
            echo '<li>Il te reste '. (48 - $temps_abs).' heures pour justifier ton absence du '.date('d/m/Y',strtotime($ligne7["dateDebut"])).' au '.date('d/m/Y',strtotime($ligne7["dateFin"])).' avant que celle-ci ne soit considérée comme injustifiée !</li>';
          }
        }
      }
	
  
      $sql8 = 'select count(*) as nb from absence where idUtilisateur='.$id.' and (justificatif is null or justificatif=\'Mot personnel\') ';
      $requete8= $connexion -> query($sql8);
      $ligne8= $requete8 -> fetch() ;
      $nb_abs= $ligne8["nb"];
      if($nb_abs >= 3){ echo '<li>Attention à ton assuidité ! </li>'  ; }
	  else {echo '<li> Continue comme ça, tu es sur la bonne voie !</li>';}
	echo '</ul>';
  } else {
      echo '<h1>Connecte-toi pour accéder aux alertes.</h1>';
  }

  ?>
  </div>
</div>