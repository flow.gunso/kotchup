<?php
  session_start();
  
  if ( !(isset($_SESSION["isConnected"])) ) {
    header("Location: /kotchup/");
  }
 
  /* debugging disparition idUtilisateur */
  $_SESSION["derniereActivite"] = time();
?>
