<?php
  /* indiquer la profondeur: rien, ../, ../../, etc */
  $profondeur = "";
  include($profondeur . "assets/php/hasConnected.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php 
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <!-- le contenu ici ! -->
      </div>
      <div class="col-md-4">
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
