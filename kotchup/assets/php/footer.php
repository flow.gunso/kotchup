<div class="navbar-fixed-bottom">
  <div id="footer">
    <p>
      kotch'up crée par Florian ANCEAU, Gaëtan DORE et Nolwenn LANNUEL avec amour, et Bootstrap.
      Aussi sur <a href="https://github.com/flowgunso/kotchup"><img src="/assets/png/GitHub_Logo.png" height="20"></a>
    </p>
  </div>
</div>


<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script>
  $( ".datepicker" ).datepicker({ dateFormat: "dd/mm/yy" });
</script>