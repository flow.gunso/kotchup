
	<!-- bannière -->
<div class="banner" id="banner">
  <a href="/"><img class="img-responsive" src="/assets/png/banner.png"></a>
</div>

<?php if ( isset($_SESSION["isConnected"]) && !empty($_SESSION["isConnected"]) ) { ?>
<!-- navigation -->
<nav class="navbar navbar-default bandeau" role="navigation">
  <div class="container">
    <!-- menu absences -->
    <div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background-color:#D9E7F2">
        Absences <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/absences/">Récapitulatif des absences</a></li>
        <li><a href="/absences/changer/?action=ajouter">Ajouter une absence</a></li>
      </ul>
    </div>
    <div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background-color:#D9E7F2">
        Devoirs <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/devoirs/">Récapitulatif des devoirs</a></li>
        <li><a href="/devoirs/changer/?action=ajouter">Ajouter un devoir</a></li>
        <li><a href="/devoirs/agenda/">Consulter l'agenda des devoirs</a></li>
      </ul>
    </div>
    <div class="btn-group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="background-color:#D9E7F2">
        Candidatures de stages <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/stages/">Récapitulatif des candidatures de stages</a></li>
        <li><a href="/stages/changer/?action=ajouter">Ajouter une candidature de stage</a></li>
      </ul>
    </div>
    <div class="btn-group pull-right">
        <label>Bienvenue, <?php echo $_SESSION["pseudo"]; ?> !&nbsp;&nbsp;<a class="btn btn-default" style="background:#da9091" href="/assets/php/disconnect.php"><span class="glyphicon glyphicon-off"></span></a></label>
    </div>
  </div>
</nav>
<?php } ?>
