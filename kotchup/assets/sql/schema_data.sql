-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2016 at 11:06 AM
-- Server version: 5.5.47-0+deb8u1
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kot_flau_org`
--

-- --------------------------------------------------------

--
-- Table structure for table `absence`
--

CREATE TABLE IF NOT EXISTS `absence` (
`idAbsence` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `motif` varchar(150) NOT NULL,
  `justificatif` varchar(100) DEFAULT NULL,
  `dateJustification` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `devoir`
--

CREATE TABLE IF NOT EXISTS `devoir` (
`idDevoir` int(11) NOT NULL,
  `idUtilisateur` int(5) NOT NULL,
  `dateDevoir` date NOT NULL,
  `infoSupp` varchar(50) NOT NULL,
  `typeDevoir` varchar(5) NOT NULL,
  `modalite` varchar(50) NOT NULL,
  `rattrapage` varchar(3) NOT NULL,
  `idModule` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
`idModule` int(2) NOT NULL,
  `labelModule` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`idModule`, `labelModule`) VALUES
(1, 'Mathématiques ou Economie'),
(2, 'Statistique descriptive 1'),
(3, 'Projet Personnel et Professionnel 1'),
(4, 'Statistique descriptive 2'),
(5, 'Probabilités et simulations 1'),
(6, 'Etudes statistiques et enquêtes'),
(7, 'Mathématiques pour les probabilités et la statisti'),
(8, 'Bases de la programmation'),
(9, 'Exploitation de données'),
(10, 'Outils de pilotage 1'),
(11, 'Economie générale et connaissance de l''entreprise'),
(12, 'Bases de la communication'),
(13, 'Initiation à l''anglais de spécialité'),
(14, 'Initiation à la statistique inférentielle'),
(15, 'Ajustement de courbes et séries chronologiques'),
(16, 'Probabilités et simulations 2'),
(17, 'Mathématiques pour l’analyse des données'),
(18, 'Développement logiciel et technologies web'),
(19, 'Structuration des données'),
(20, 'Programmation statistique 1'),
(21, 'Outils de pilotage 2'),
(22, 'Economie générale et management des organisations'),
(23, 'Communication, information et argumentation'),
(24, 'Approfondissement de l’anglais de spécialité'),
(25, 'Projet Personnel et Professionnel 2'),
(26, 'Conduite de projets'),
(27, 'Projet 1'),
(28, 'Analyse des données'),
(29, 'Estimation et tests d''hypothèse'),
(30, 'Modèle linéaire'),
(31, 'Système d’information décisionnel'),
(32, 'Développement d’applications décisionnelles'),
(33, 'Techniques de gestion pour la décision'),
(34, 'Economie'),
(35, 'Communication professionnelle'),
(36, 'Anglais professionnel et coopération international'),
(37, 'Projet Personnel et Professionnel 3'),
(38, 'Etude de cas en statistique et informatique décisi'),
(39, 'Domaines d’application 1'),
(40, 'Programmation statistique 2'),
(41, 'Projet 2'),
(42, 'Data mining'),
(43, 'Sondages'),
(44, 'Bases de données avancées'),
(45, 'Economie, gestion et droit'),
(46, 'Communication dans les organisations'),
(47, 'Anglais scientifique'),
(48, 'Domaines d’application 2'),
(49, 'Projet 3'),
(50, 'Stage – application'),
(51, 'Stage – restitution');

-- --------------------------------------------------------

--
-- Table structure for table `relance`
--

CREATE TABLE IF NOT EXISTS `relance` (
`idRelance` int(11) NOT NULL,
  `moyen` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `idStage` int(11) NOT NULL,
  `date_rel` date NOT NULL,
  `reponse` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `semestre`
--

CREATE TABLE IF NOT EXISTS `semestre` (
`idSemestre` int(2) NOT NULL,
  `labelSemestre` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `semestre`
--

INSERT INTO `semestre` (`idSemestre`, `labelSemestre`) VALUES
(1, 'Semestre 1'),
(2, 'Semestre 2'),
(3, 'Semestre 3'),
(4, 'Semestre 4');

-- --------------------------------------------------------

--
-- Table structure for table `semUE`
--

CREATE TABLE IF NOT EXISTS `semUE` (
  `idSemestre` int(2) NOT NULL,
  `idUE` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `semUE`
--

INSERT INTO `semUE` (`idSemestre`, `idUE`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(4, 13),
(4, 14),
(4, 15);

-- --------------------------------------------------------

--
-- Table structure for table `stageent`
--

CREATE TABLE IF NOT EXISTS `stageent` (
`idStage` int(11) NOT NULL,
  `nomOrg` varchar(50) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `libelle_mis` varchar(100) NOT NULL,
  `reponse` varchar(10) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `date_dem` date NOT NULL,
  `moyen` varchar(50) NOT NULL,
  `date_rec` date NOT NULL,
  `genre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ue`
--

CREATE TABLE IF NOT EXISTS `ue` (
`idUE` int(2) NOT NULL,
  `labelUE` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ue`
--

INSERT INTO `ue` (`idUE`, `labelUE`) VALUES
(1, 'Accueil'),
(2, 'Statistique et outils mathématiques'),
(3, 'Introduction à l’informatique et au décisionnel'),
(4, 'Environnement économique et communication'),
(5, 'Introduction à la statistique inférentielle'),
(6, 'Bases de l’informatique et du décisionnel'),
(7, 'Economie, management et communication'),
(8, 'Projet'),
(9, 'Statistique'),
(10, 'Solutions décisionnelles'),
(11, 'Environnement économique et professionnel'),
(12, 'Application professionnelle'),
(13, 'Statistique et informatique décisionnelle'),
(14, 'Environnement professionnel et domaines d’application'),
(15, 'Projet et stage');

-- --------------------------------------------------------

--
-- Table structure for table `ueModule`
--

CREATE TABLE IF NOT EXISTS `ueModule` (
  `idUE` int(2) NOT NULL,
  `idModule` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `ueModule`
--

INSERT INTO `ueModule` (`idUE`, `idModule`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(3, 8),
(3, 9),
(3, 10),
(4, 11),
(4, 12),
(4, 13),
(5, 14),
(5, 15),
(5, 16),
(5, 17),
(6, 18),
(6, 19),
(6, 20),
(6, 21),
(7, 22),
(7, 23),
(7, 24),
(7, 25),
(8, 26),
(8, 27),
(9, 28),
(9, 29),
(9, 30),
(10, 31),
(10, 32),
(10, 33),
(11, 34),
(11, 35),
(11, 36),
(11, 37),
(12, 38),
(12, 39),
(12, 40),
(12, 41),
(13, 42),
(13, 43),
(13, 44),
(14, 45),
(14, 46),
(14, 47),
(14, 48),
(15, 49),
(15, 50),
(15, 51);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
`idUtilisateur` int(11) NOT NULL,
  `pseudo` varchar(15) NOT NULL,
  `mp` varchar(128) NOT NULL,
  `mail` varchar(55) NOT NULL,
  `recoveryToken` varchar(128) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absence`
--
ALTER TABLE `absence`
 ADD PRIMARY KEY (`idAbsence`);

--
-- Indexes for table `devoir`
--
ALTER TABLE `devoir`
 ADD PRIMARY KEY (`idDevoir`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
 ADD PRIMARY KEY (`idModule`);

--
-- Indexes for table `relance`
--
ALTER TABLE `relance`
 ADD PRIMARY KEY (`idRelance`);

--
-- Indexes for table `semestre`
--
ALTER TABLE `semestre`
 ADD PRIMARY KEY (`idSemestre`);

--
-- Indexes for table `semUE`
--
ALTER TABLE `semUE`
 ADD PRIMARY KEY (`idSemestre`,`idUE`);

--
-- Indexes for table `stageent`
--
ALTER TABLE `stageent`
 ADD PRIMARY KEY (`idStage`);

--
-- Indexes for table `ue`
--
ALTER TABLE `ue`
 ADD PRIMARY KEY (`idUE`);

--
-- Indexes for table `ueModule`
--
ALTER TABLE `ueModule`
 ADD PRIMARY KEY (`idUE`,`idModule`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
 ADD PRIMARY KEY (`idUtilisateur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absence`
--
ALTER TABLE `absence`
MODIFY `idAbsence` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `devoir`
--
ALTER TABLE `devoir`
MODIFY `idDevoir` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
MODIFY `idModule` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `relance`
--
ALTER TABLE `relance`
MODIFY `idRelance` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semestre`
--
ALTER TABLE `semestre`
MODIFY `idSemestre` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stageent`
--
ALTER TABLE `stageent`
MODIFY `idStage` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ue`
--
ALTER TABLE `ue`
MODIFY `idUE` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
