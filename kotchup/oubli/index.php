﻿<?php
  /* indiquer la profondeur: rien, ../, ../../, etc */
  $profondeur = "../"; $error = "";
  
  if ( isset($_POST["subRecovery"])) {
    include($profondeur . "assets/php/mysql.php");
    $query = $connexion -> prepare("SELECT * FROM utilisateur WHERE mail=?");
    $query -> execute(array($_POST["mail"]));
    $mail = $query -> fetch(PDO::FETCH_ASSOC);
    
    /* mail existant */
    if ( $mail ) {
      $recoveryToken = hash('sha512', date('U'));
      $query = $connexion -> prepare("UPDATE `utilisateur` SET `recoveryToken`=? WHERE `mail`=?");
      $query -> execute(array($recoveryToken, $_POST["mail"]));

      $headers   = array();
      $headers[] = "MIME-Version: 1.0";
      $headers[] = "Content-Transfer-Encoding: 8bit";
      $headers[] = "Content-Type: text/html; charset=UTF-8";
      $headers[] = "From: noreply@univ-poitiers.fr";
      
      $message = "Tu viens d'effectué une demande de nouveau mot de passe.</br>";
      $message .= 'Clique sur le <a href="http://stid7.dut-stid-niort.fr/oubli/?recoveryToken=' .$recoveryToken. '">lien</a> pour definir un nouveau mot de passe.</br>';
      
      $sujet = "Mot de passe oublié sur kotchup!";
      mail($_POST["mail"], '=?utf-8?B?'.base64_encode($sujet).'?=', $message, implode("\r\n", $headers));
      $error = "Mail envoyé.";
    } else { $error = "Email introuvable. Êtes-vous bien inscrit ?"; }
    
  } else if ( isset($_POST["subNewPwd"])) {
    include($profondeur . "assets/php/mysql.php");
    $query = $connexion -> prepare("SELECT pseudo FROM utilisateur WHERE `recoveryToken`=?");
    $query -> execute(array($_GET["recoveryToken"]));
    $user = $query -> fetch(PDO::FETCH_ASSOC);
    $pwd = hash('sha512', $_POST["mdp"] . $user["pseudo"]);
    $query = $connexion -> prepare("UPDATE `utilisateur` SET `recoveryToken`=NULL, `mp`=? WHERE `recoveryToken`=?");
    $query -> execute(array($pwd, $_GET["recoveryToken"]));
    header("Location: ..");
  }
  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php 
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <?php
          if ( isset($_GET["recoveryToken"]) ) {
            include($profondeur . "assets/php/mysql.php");
            $query = $connexion -> prepare("SELECT * FROM utilisateur WHERE recoveryToken=?");
            $query -> execute(array($_GET["recoveryToken"]));
            $recoveryToken = $query -> fetch(PDO::FETCH_ASSOC);
            
            if ( $recoveryToken ) {
              echo '<form class="form-signin" method="post" action="?recoveryToken=' .$_GET["recoveryToken"]. '">';
              echo '<h1 class="form-signin-heading">Entre un nouveau mot de passe</h1><a href="..">retour</a>';  
              echo '<label for="mdp" class="sr-only">Mot de passe</label>';
              echo '<input type="password" id="mdp" name="mdp" class="form-control" placeholder="mot de passe" required>';
              echo '<label for="mdpCheck" class="sr-only">Mot de passe à nouveau</label>';
              echo '<input type="password" id="mdpCheck" name="mdpCheck" class="form-control" placeholder="mot de passe à nouveau" required>';
              echo '<input type="hidden" name="pseudo" value="' .$recoveryToken["pseudo"]. '">';
              echo '<button class="btn btn-lg btn-primary btn-block" type="submit" name="subNewPwd">Utiliser ce mot de passe</button>';
              echo '</form>';
            } else {
              header("Location: ..");
            }
          } else {
            echo '<form class="form-signin" method="post" action="">';
            echo '<h1 class="form-signin-heading">Mot de passe oublié ?</h1><a href="..">retour</a>';
            echo '<label for="mail" class="sr-only">Votre mail ?</label>';
            echo '<input type="email" id="mail" name="mail" class="form-control" placeholder="email universitaire" required autofocus>';
            echo '<button class="btn btn-lg btn-primary btn-block" type="submit" name="subRecovery">Demander</button>';
            echo '</form>';
          } 
        ?>
        <span class="errors"><?php echo $error; ?></span>
     </div>
      
      <?php include($profondeur . "assets/php/alertes.php"); ?>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
