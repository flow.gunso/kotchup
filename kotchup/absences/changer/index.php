<?php
  /* indiquer la profondeur: rien, ../, ../../, etc */
  $profondeur = "../../"; $err = "";
  include($profondeur . "assets/php/hasConnected.php");
  
  /* protège des entrées non-sollicitées */
  if ( !isset( $_GET["action"]) && !isset($_GET["id"])  ||  !isset($_GET["action"]) && isset($_GET["id"])  ||  $_GET["action"]!="ajouter" && !isset($_GET["id"])  ||  $_GET["action"]=="ajouter" && isset($_GET["id"]) ) { 
    header("Location: .." );
  }
  
  /* récupère la requête GET actuelle */
  $getRequest = "?action=" . $_GET["action"];
  if( isset($_GET["id"]) ) {
    $getRequest .= "&id=" . $_GET["id"];
  }
  
  /* formulaire envoyé */
  if ( isset($_POST["subAbsence"]) ) {
    include($profondeur . "assets/php/mysql.php");
    
    /* correctif date en français */
    if ( $_GET["action"] == "ajouter" ) {
      $_POST["dateDebut"] = str_replace("/","-",$_POST["dateDebut"]);
      $_POST["dateFin"] = str_replace("/","-",$_POST["dateFin"]);
      $_POST["dateDebut"] = date("Y-m-d", strtotime($_POST["dateDebut"]));
      $_POST["dateFin"] = date("Y-m-d", strtotime($_POST["dateFin"]));
      if ( isset($_POST["hasJustif"]) ) {
        $_POST["dateJustif"] = str_replace("/","-",$_POST["dateJustif"]);
        $_POST["dateJustif"] = date("Y-m-d", strtotime($_POST["dateJustif"]));
      }
    } elseif ( $_GET["action"] == "modifier" ) {
      $_POST["dateJustif"] = str_replace("/","-",$_POST["dateJustif"]);
      $_POST["dateJustif"] = date("Y-m-d", strtotime($_POST["dateJustif"]));
    }
    
    /* formulaire d'ajout */
    if ( $_GET["action"] == "ajouter" ) {
      if ( !(strtotime($_POST["dateDebut"]) > strtotime($_POST["dateFin"])) ) {
        /* avec justificatif */
        if ( isset($_POST["hasJustif"]) ) {
          $query = $connexion -> prepare("INSERT INTO absence (`idUtilisateur`, `dateDebut`, `dateFin`, `motif`, `justificatif`, `dateJustification`) VALUES ( ?, ?, ?, ?, ?, ?)");
          $query -> execute(array($_SESSION["idUtilisateur"], $_POST["dateDebut"], $_POST["dateFin"], $_POST["motif"], $_POST["typeJustif"], $_POST["dateJustif"]));
        /* sans justificatif */
        } else {
          $query = $connexion -> prepare("INSERT INTO absence (`idUtilisateur`, `dateDebut`, `dateFin`, `motif`, `justificatif`, `dateJustification`) VALUES (?, ?, ?, ?, NULL, NULL)");
          $query -> execute(array($_SESSION["idUtilisateur"], $_POST["dateDebut"], $_POST["dateFin"], $_POST["motif"]));
        }
        header("Location: ..");
      } else { $err = "Vos dates d'absence sont invalides."; }
      
    
    /* formulaire de modification */
    } else if ( $_GET["action"] == "modifier" ) {
      $query = $connexion -> prepare("UPDATE absence SET justificatif=?, dateJustification=? WHERE idAbsence=?");
      $query -> execute(array($_POST["typeJustif"], $_POST["dateJustif"], $_GET["id"]));
      header("Location: ..");
      
    /* formulaire de suppression */    
    } else if ( $_GET["action"] == "supprimer" ) {
      $query = $connexion -> prepare("DELETE FROM absence WHERE idUtilisateur=? AND idAbsence=?");
      $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
      header("Location: ..");
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php include($profondeur . "assets/php/navigation.php"); ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
      
        <!-- contenu -->
        <div class="col-md-8">
          
          <form class="form-group formulaire" method="post" action="<?php echo $getRequest ?>">
          <h1><?php echo ucfirst($_GET["action"]) ?> une absence</h1>&nbsp;&nbsp;<a href="..">retour</a></br>
            <?php
            
              $justifs = array("certificat médical", "convocation administrative", "mot personnel", "certificat de décès");
              if ( $_GET["action"] == "supprimer" || $_GET["action"] == "modifier" ) {
                
                /* verification de l'existence de la donnée */
                include($profondeur . "assets/php/mysql.php");
                $query = $connexion -> prepare("SELECT * FROM absence WHERE idUtilisateur=? AND idAbsence=?");
                $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
                $absence = $query -> fetch();
                
                if ( $absence ) {
                  /* correctif date en français */
                  $absence["dateDebut"] = date("d/m/Y", strtotime($absence["dateDebut"]));
                  $absence["dateFin"] = date("d/m/Y", strtotime($absence["dateFin"]));
                  $absence["dateJustification"] = date("d/m/Y", strtotime($absence["dateJustification"]));
                  
                  /* formulaire de suppression */
                  if ( $_GET["action"] == "supprimer" ) {
                    echo "Attention, suppression de l'absence du " .$absence["dateDebut"]. " au " .$absence["dateFin"]. " !</br>";
                    
                  /* formulaire de modification */
                  } else {                 
                    echo'<label>Début </label></br><input type="text" name="dateDebut" class="datepicker" value="'.$absence["dateDebut"].'" disabled/></br>';
                    echo'<label>Fin </label></br><input type="text" name="dateFin" class="datepicker" value="'. $absence["dateFin"].'" disabled/></br>';
                    echo'<label>Motif </label></br><input type="text" name="motif" value="'. $absence["motif"].'" disabled/></br>';
                    echo'<label>Type </label></br><select name="typeJustif">';
                    foreach ( $justifs as $justif ) { echo '<option value="' .$justif. '">' .ucfirst($justif). '</option>'; } 
                    echo'</select></br>';
                    echo'<label>Le </label></br><input type="text" name="dateJustif" class="datepicker" required/></br> ';
                }
                } else { header("Location: .."); }

              /* formulaire d'ajout */
              } else if ( $_GET["action"] == "ajouter" ) { 
                echo'<label>Début </label></br><input type="text" name="dateDebut" class="datepicker" required/></br>';
                echo'<label>Fin </label></br><input type="text" name="dateFin" class="datepicker" required/></br>';
                echo'<label>Motif </label></br><input type="text" name="motif" required/></br>';
                echo'<label>L\'absence est-elle justifiée ?</label>&nbsp;<input type="checkbox" name="hasJustif"></br>';
                echo'<span id="spanJustif">';
                echo'<label>Type </label></br><select name="typeJustif">';
                foreach ( $justifs as $justif ) { echo '<option value="' .$justif. '">' .ucfirst($justif). '</option>'; } 
                echo'</select></br>';
                echo'<label>Le </label></br><input type="text" name="dateJustif" class="datepicker"/></br>';
                echo'</span>'; 
                
              } else { header("Location: .."); }
			  ?>
               <input type="submit" class="btn btn-default" name="subAbsence" value="<?php echo ucfirst($_GET["action"]) ?>">
          </form>
          <i class="errors"><?php echo $err ?></i>
        </div>
        
        <!-- alertes -->
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>
  
  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>

<!-- script de cache du justificatif -->
<script>
  $('#spanJustif').hide();
  $('input[name="hasJustif"]').change(function() {
    if(this.checked) {
      $('#spanJustif').show();
    } else {
      $('#spanJustif').hide();
    }
  });
</script>
</html>
