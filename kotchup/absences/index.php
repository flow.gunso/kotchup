<?php
  $profondeur = "../";
  include($profondeur . "assets/php/hasConnected.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php 
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php
            /* verifie l'existence d'absence */
            include($profondeur . "assets/php/mysql.php");
            $query = $connexion -> prepare("SELECT * FROM absence WHERE idUtilisateur=?");
            $query -> execute(array($_SESSION["idUtilisateur"]));
            $hasAbsence = $query -> fetch();
            if ( $hasAbsence ) {
              echo '<h1>Mes absences</h1>';
              
              /* absences justifiées */
              $query = $connexion -> prepare("SELECT * FROM absence WHERE idUtilisateur=? AND justificatif IS NOT NULL");
              $query -> execute(array($_SESSION["idUtilisateur"]));
              if ( $query -> rowCount() > 0 ) {
                echo '<h3>Absences justifiées</h3>';
                echo '<div class="table-responsive"><table class="resume table">';
                echo '<tr><th>Du</th><th>Au</th><th>Motif</th><th>Justificatif</th><th>Le</th><th></th></tr>';
                while ( $rows = $query -> fetch(PDO::FETCH_ASSOC) ) {
                  $rows["dateDebut"] = date("d/m/Y", strtotime($rows["dateDebut"]));
                  $rows["dateFin"] = date("d/m/Y", strtotime($rows["dateFin"]));
                  $rows["dateJustification"] = date("d/m/Y", strtotime($rows["dateJustification"]));
                  echo '<tr><td>' .$rows["dateDebut"]. '</td><td>' .$rows["dateFin"]. '</td><td>' .$rows["motif"]. '</td><td>' .ucfirst($rows["justificatif"]). '</td><td>' .$rows["dateJustification"]. '</td><td><a href="changer/?action=supprimer&id=' .$rows["idAbsence"]. '"><span class="glyphicon glyphicon-remove"></span></a></td></tr>';
                }
                echo '</table></div></br>';
              }
              
              /* absences non-justifiées */
              $query = $connexion -> prepare("SELECT * FROM absence WHERE idUtilisateur=? AND justificatif IS NULL");
              $query -> execute(array($_SESSION["idUtilisateur"]));
              if ( $query -> rowCount() > 0 ) {
                echo '<h3>Absences non-justifiées</h3>';
                echo '<div class="table-responsive"><table class="resume table">';
                echo '<tr><th>Du</th><th>Au</th><th>Motif</th><th></th></tr>';
                while ( $rows = $query -> fetch(PDO::FETCH_ASSOC) ) {
                  $rows["dateDebut"] = date("d/m/Y", strtotime($rows["dateDebut"]));
                  $rows["dateFin"] = date("d/m/Y", strtotime($rows["dateFin"]));
                  echo '<tr><td>' .$rows["dateDebut"]. '</td><td>' .$rows["dateFin"]. '</td><td>' .$rows["motif"]. '</td><td><a href="changer/?action=modifier&id=' .$rows["idAbsence"]. '"><span class="glyphicon glyphicon-cog"></span></a><a href="changer/?action=supprimer&id=' .$rows["idAbsence"]. '"><span class="glyphicon glyphicon-remove"></span></a></td></tr>';
                }
                echo '</table></div>';
              }
            } else { echo "<h1>Aucune absence enregistrée jusqu'à maintenant.</h1>"; }

          ?>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>
  
  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
