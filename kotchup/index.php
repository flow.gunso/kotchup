<?php
  /* definit la profondeur pour simplifier les include */
  $profondeur = "";
  $err = "";

  
  /* débute une session guest */
  session_start();
  
  /* redirige vers l'accueil si déjà connecté */
  if ( isset($_SESSION["isConnected"]) ) {
    header("Location: /accueil/");
  }

  /* formulaire envoyé */
  if ( isset($_POST["subLogin"]) ) {
    /* effectue la requête SQL */
    include($profondeur . "assets/php/mysql.php");
    $mdp = hash('sha512', $_POST["mdp"] . $_POST["pseudo"]);
    $query = $connexion -> prepare("SELECT * FROM utilisateur WHERE pseudo=? AND mp=?");
    $query -> execute(array($_POST["pseudo"], $mdp));
    $rows = $query -> fetch(PDO::FETCH_ASSOC);

     /* connecte et redirige */
    if ( $rows ) {
      $err="connecté";
      session_unset();
      session_destroy();
      session_start();
      $_SESSION["pseudo"] = $_POST["pseudo"];
      $_SESSION["idUtilisateur"] = $rows["idUtilisateur"];
      $_SESSION["isConnected"] = true;
      /* debugging disparition idUtilisateur */
      $_SESSION["derniereActivite"] = time();
      header("Location: /accueil/");
    } else { $err = "Etudiant introuvable, verifiez vos informations."; }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body >
  <!-- header & navigation -->
  <?php
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container corps">
      <div class="row">
        <div class="col-md-8">
          <form class="form-signin" method="post">
            <h1 class="form-signin-heading">Connectez-vous</h1>
            <label for="pseudo" class="sr-only">Pseudo</label>
            <input type="text" id="pseudo" name="pseudo" class="form-control" placeholder="pseudo" required autofocus>
            <label for="mdp" class="sr-only">Mot de passe</label>
            <input type="password" id="mdp" name="mdp" class="form-control" placeholder="mot de passe" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="subLogin">Se connecter</button>
          </form>
          <i>Vous n'avez pas de compte ? Créez en un <a href="inscription/">ici</a>.</i></br>
            <i>Ou mot de passe perdu ? Récupérez-le <a href="oubli/">ici</a>.</i></br>
          <i class="errors"><?php echo $err ?></i>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
