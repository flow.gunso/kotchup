<?php
  /* indiquer la profondeur: rien, ../, ../../, etc */
  $profondeur = "../../"; $err = "";
  include($profondeur . "assets/php/hasConnected.php");
  
  /* protège des entrées non-sollicitées */
  if ( !isset( $_GET["action"]) && !isset($_GET["id"])  ||  !isset($_GET["action"]) && isset($_GET["id"])  ||  $_GET["action"]!="ajouter" && !isset($_GET["id"])  ||  $_GET["action"]=="ajouter" && isset($_GET["id"]) ) { 
    header("Location: /stages/" );
  }
  
  /* récupère la requête GET actuelle */
  $getRequest = "?action=" . $_GET["action"];
  if( isset($_GET["id"]) ) {
    $getRequest .= "&id=" . $_GET["id"];
  }
  
  /* formulaire envoyé */
  if ( isset($_POST["subStageChanger"]) ) {
    include($profondeur . "assets/php/mysql.php");
    
    /* correctif date en français */
    if ( $_GET["action"] == "ajouter")  {
      $_POST["date_dem"] = str_replace("/","-",$_POST["date_dem"]);
      $_POST["date_dem"] = date("Y-m-d", strtotime($_POST["date_dem"]));
      if ( isset($_POST["hasRep"]) ) {
        $_POST["date_rec"] = str_replace("/","-",$_POST["date_rec"]);
        $_POST["date_rec"] = date("Y-m-d", strtotime($_POST["date_rec"]));
      }
    } else if ($_GET["action"] == "modifier") {
	  $_POST["date_rec"] = str_replace("/","-",$_POST["date_rec"]);
      $_POST["date_rec"] = date("Y-m-d", strtotime($_POST["date_rec"])); 
	  $_POST["date_dem"] = str_replace("/","-",$_POST["date_dem"]);
	  $_POST["date_dem"] = date("Y-m-d", strtotime($_POST["date_dem"]));
	  } 
    /* formulaire d'ajout */ 
    if ( $_GET["action"] == "ajouter" ) {
        /* avec réponse */
        if ( isset($_POST["hasRep"]) ) {	
		echo 'repondu';
		    if ( !(strtotime($_POST["date_dem"]) > strtotime($_POST["date_rec"])) ) {
               $query = $connexion -> prepare("INSERT INTO stageent (`idUtilisateur`, `nomOrg`, `libelle_mis`, `lieu`, `date_dem`, `moyen`, `genre`, `reponse`, `date_rec`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)");
               $query -> execute(array($_SESSION["idUtilisateur"], $_POST["nomOrg"], $_POST["libelle_mis"], $_POST["lieu"], $_POST["date_dem"], $_POST["moyen"], $_POST["genre"], $_POST["reponse"], $_POST["date_rec"]));
			   }
			else { $err = "Vos dates de reception et d'envoi de candidature sont invalides."; }
		}
		else {
			 $_POST["reponse"] = 'En cours' ; 
			 $_POST["date_rec"] = '0000-00-00';
			 $query = $connexion -> prepare("INSERT INTO stageent (`idUtilisateur`, `nomOrg`, `libelle_mis`, `lieu`, `date_dem`, `moyen`, `genre`, `reponse`, `date_rec`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)");
             $query -> execute(array($_SESSION["idUtilisateur"], $_POST["nomOrg"], $_POST["libelle_mis"], $_POST["lieu"], $_POST["date_dem"], $_POST["moyen"], $_POST["genre"], $_POST["reponse"], $_POST["date_rec"]));
		     header("Location: ..");
		} 
      
    
    /* formulaire de modification */
      } else if ( $_GET["action"] == "modifier" ) {
	  include($profondeur . "assets/php/mysql.php");
	  if ( !( strtotime($_POST["date_dem"]) > strtotime($_POST["date_rec"]) )) {
      $query = $connexion -> prepare("UPDATE stageent SET reponse=?, date_rec=? WHERE idStage=?");
	  $query -> execute(array( $_POST["reponse"], $_POST["date_rec"], $_GET["id"]));
	  }
	  else { $err = "Vos dates de reception et d'envoi de candidature sont invalides."; }
	  header("Location: ..");
      
    /* formulaire de suppression */    
    } else if ( $_GET["action"] == "supprimer" ) {
      include($profondeur . "assets/php/mysql.php");
	  $query = $connexion -> prepare("DELETE FROM stageent WHERE idUtilisateur=? AND idStage=?");
      $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
      header("Location: ..");
	  /* formulaire de relance */
    } else if ($_GET["action"] == "relancer" ) {
	  include($profondeur . "assets/php/mysql.php");
	  $query = $connexion -> prepare("insert into relance (moyen,idStage,date_rel,reponse) values (?, ?, ?, ?)");
	  $query -> execute(array($_POST["moy"], $_GET["id"], date("Y-m-d",time()), $_POST["reponse"]));
	  header("Location: ..");
	}
	}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php include($profondeur . "assets/php/navigation.php"); ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
      
        <!-- contenu -->
        <div class="col-md-8">
          
          <form class="form-group formulaire" method="post" action="<?php echo $getRequest ?>">
          <h1><?php echo ucfirst($_GET["action"]) ?> une candidature de stage</h1>&nbsp;&nbsp;<a href="..">retour</a></br>
            <?php
            
              $genre = array("Réponse offre", "Candidature spontanée");
        $moyen = array("E-Mail","Lettre","Entretien");
        $reponse = array("Acceptée","Refusée");
        $reponse2 = array("En cours","Acceptée", "Refusée");
        $moy = array("E-Mail","Lettre","Téléphone");
              if ( $_GET["action"] == "supprimer" || $_GET["action"] == "modifier" ) {
                
                /* verification de l'existence de la donnée */
                include($profondeur . "assets/php/mysql.php");
                $query = $connexion -> prepare("SELECT * FROM stageent WHERE idUtilisateur=? AND idStage=?");
                $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
                $stage = $query -> fetch();
                
                /* correctif date en français */
                $stage["date_dem"] = date("d/m/Y", strtotime($stage["date_dem"]));
                $stage["date_rec"] = date("d/m/Y", strtotime($stage["date_rec"]));
                
                if ( $stage ) {
                  /* formulaire de suppression */
                  if ( $_GET["action"] == "supprimer" ) {
                    echo "Attention, suppression de la candidature de stage faite à  " .$stage["nomOrg"]. " à " .$stage["lieu"]. " pour " .$stage["nom_libelle"]. " !</br>";                  
                  /* formulaire de modification */
                  } else {
                    echo '
                      <label>Nom organisation <input type="text" name="nomOrg" value="' .$stage["nomOrg"]. '" disabled /></label></br>
                      <label>Lieu<input type="text" name="lieu" value="' .$stage["lieu"].'" disabled /> </label></br>
                      <label>Titre de la mission<input type="text" name="libelle_mis" value="'.$stage["libelle_mis"].'" disabled /> </label></br>
                      <label>Date de ta candidature : <input type="text" name="date_dem" class="datepicker" value="' .$stage["date_dem"].'" disabled /></label></br>
                      <label>Type de candidature <select name="genre" disabled >';
                    foreach ( $genre as $genre ) { echo '<option value="' .$genre. '">' .ucfirst($genre). '</option>'; }
                    echo '
                      </select></label></br>
                      <label>Moyen de transmission <select name="moyen" disabled>';
                    foreach ( $moyen as $moyen ) { echo '<option value="' .$moyen. '">' .ucfirst($moyen). '</option>'; }
                    echo '
                      </select></label></br>
                      <label>Réponse <select name="reponse">';
                    foreach ( $reponse as $reponse ) { echo '<option value="' .$reponse. '">' .ucfirst($reponse). '</option>'; }
                    echo '
                      </select></label></br>
                      <input type="hidden" name="date_dem" value="'.$stage["date_dem"].'" /></br>
                      <label>Date de la réponse à ta candidature <input type="text" name="date_rec" class="datepicker" value="'.$stage["date_dem"].'" /></label></br>';
                  }
                } else { header("Location: .."); }

              /* formulaire d'ajout */
              } else if ( $_GET["action"] == "ajouter" ) { ?>
                <label>Nom organisation </label></br><input type="text" name="nomOrg" required/></br>
                <label>Lieu</label></br><input type="text" name="lieu" required/></br>
                <label>Titre de la mission </label></br><input type="text" name="libelle_mis" required/></br>
                <label>Date de ta candidature </label></br><input type="text" name="date_dem" class="datepicker" /></br>
                <label>Type de candidature </label></br><select name="genre">
                <?php foreach ($genre as $genre) {echo '<option value="' .$genre.'">' . ucfirst($genre).'</option>';} ?>
                </select></br>
                <label>Moyen de transmission </label></br><select name="moyen">
                <?php foreach ($moyen as $moyen) {echo '<option value="' .$moyen.'">' . ucfirst($moyen).'</option>';} ?>
                </select></br>
                <label>As-tu reçu une réponse à ta candidature ?</label>&nbsp;<input type="checkbox" name="hasRep"></br>
                <span id="spanRep">
                <label>Réponse: </label></br><select name="reponse">
                  <?php foreach ( $reponse as $reponse ) { echo '<option value="' .$reponse. '">' .ucfirst($reponse). '</option>'; } ?>
                </select></br>
                <label>Date de la réponse à ta candidature </label></br><input type="text" name="date_rec" class="datepicker"/></br>
                </span> 
                <!-- Formulaire de relance --> 

        <?php } else{ header("Location: .."); }
            ?> 
            <input type="submit" class="btn btn-default" name="subStageChanger" value="<?php echo ucfirst($_GET["action"]) ?>">
          </form>
          <i class="errors"><?php echo $err ?></i>
        </div>
        
        <!-- alertes -->
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>

<!-- script de cache du justificatif -->
<script>
  $('#spanRep').hide();
  $('input[name="hasRep"]').change(function() {
    if(this.checked) {
      $('#spanRep').show();
    } else {
      $('#spanRep').hide();
    }
  });
</script>
</html>
