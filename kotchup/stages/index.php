<?php
  /* definit la profondeur pour simplifier les include */
  $profondeur = "../";
  include($profondeur . "assets/php/hasConnected.php");
  
  /* connexion à la base de données */
  include($profondeur . "assets/php/mysql.php");
  $idUtilisateur = $_SESSION['idUtilisateur'];
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php 
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php
          /* verifie l'existence de candidature */
          include($profondeur. "assets/php/mysql.php");
          $query = $connexion -> prepare("Select * from stageent where idUtilisateur=?");
          $query -> execute(array($_SESSION["idUtilisateur"]));
          $hasCand = $query -> fetch();
          if ( $hasCand ) {
            echo '<h1> Mes candidatures de stages </h1>';
            
            /*Création du tableau des demandes en cours*/
            $query = $connexion -> prepare("SELECT * FROM stageent WHERE idUtilisateur=? AND reponse=?");
            $query -> execute(array($idUtilisateur, "En cours"));
            if ( $query -> rowCount() > 0 ) {
              echo '<h3> Mes candidatures en cours </h3>' ;
              echo '<div class="table-responsive"><table class="resume table">';
              echo '<tr><th>Organisation</th><th>Lieu</th><th>Mission</th><th>Date envoi</th><th>Mode envoi</th><th>Type candidature</th><th></th></tr>';
              while ( $ligne = $query -> fetch() ) {
                echo '<tr><td>'.$ligne["nomOrg"].'</td><td>'.$ligne["lieu"].'</td><td>'.$ligne["libelle_mis"].'</td><td>'.date("d/m/Y",strtotime($ligne["date_dem"])).'</td><td>'.$ligne["moyen"].'</td><td>'.$ligne["genre"].'</td>';
                echo '<td style="white-space: nowrap;"><a href="changer/?action=modifier&id='.$ligne["idStage"].'"><span class="glyphicon glyphicon-cog"></span></a> <a href="changer/?action=supprimer&id='.$ligne["idStage"].'"><span class="glyphicon glyphicon-remove"></span></a></td></tr>';
              }
              echo '</table></div></br>';
            }
    
            /*Création du tableau des demandes en refusées*/
            $query = $connexion -> prepare("SELECT * FROM stageent WHERE idUtilisateur=? AND reponse=?");
            $query -> execute(array($idUtilisateur, "Refusée"));
            if ( $query -> rowCount() > 0 ) {
              echo '<h3> Mes candidatures refusées </h3>' ;
              echo '<div class="table-responsive"><table class="resume table">';
              echo '<tr><th>Organisation</th><th>Lieu</th><th>Mission</th><th>Date envoi</th><th>Mode envoi</th><th>Type candidature</th><th>Date Reception</th><th></th></tr>';
              while ( $ligne = $query -> fetch() ) {
                echo '<tr><td>'.$ligne["nomOrg"].'</td><td>'.$ligne["lieu"].'</td><td>'.$ligne["libelle_mis"].'</td><td>'.date("d/m/Y",strtotime($ligne["date_dem"])).'</td><td>'.$ligne["moyen"].'</td><td>'.$ligne["genre"].'</td><td>'.date("d/m/Y",strtotime($ligne["date_rec"])).'</td>';
                echo '<td><a href="changer/?action=supprimer&id='.$ligne["idStage"].'"><span class="glyphicon glyphicon-remove"></span></a></td></tr>';
              }
              echo '</table></div></br>';
            }
        
            /*Création du tableau des demandes en acceptées*/
            $query = $connexion -> prepare("SELECT * FROM stageent WHERE idUtilisateur=? AND reponse=?");
            $query -> execute(array($idUtilisateur, "Acceptée"));
            if ( $query -> rowCount() > 0 ) {
            echo '<h3> Mes candidatures acceptées </h3> ' ;
            echo '<div class="table-responsive"><table class="resume table">';
            echo '<tr><th>Organisation</th><th>Lieu</th><th>Mission</th><th>Date envoi</th><th>Mode envoi</th><th>Type candidature</th><th>Date Reception</th><th></th></tr>';
            while ( $ligne = $query -> fetch() ) {
              echo '<tr><td>'.$ligne["nomOrg"].'</td><td>'.$ligne["lieu"].'</td><td>'.$ligne["libelle_mis"].'</td><td>'.date("d/m/Y",strtotime($ligne["date_dem"])).'</td><td>'.$ligne["moyen"].'</td><td>'.$ligne["genre"].'</td><td>'.date("d/m/Y",strtotime($ligne["date_rec"])).'</td>';
              echo '<td><a href="changer/?action=supprimer&id='.$ligne["idStage"].'"><span class="glyphicon glyphicon-remove"/></span></a></td></tr>';
            }
            echo '</table></div></br>';
          }
          } else { echo "<h1>Aucune candidature envoyée à ce jour. </h1>";}
          ?>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
