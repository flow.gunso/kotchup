<?php
  /* indiquer la profondeur: rien, ../, ../../, etc */
  $profondeur = "../../"; $err = "";
  include($profondeur . "assets/php/hasConnected.php");
  
  /* protège des entrées non-sollicitées */
  if ( !isset( $_GET["action"]) && !isset($_GET["id"])  ||  !isset($_GET["action"]) && isset($_GET["id"])  ||  $_GET["action"]!="ajouter" && !isset($_GET["id"])  ||  $_GET["action"]=="ajouter" && isset($_GET["id"]) ) {
    header("Location: .." );
  }
  
  /* récupère la requête GET actuelle */
  $getRequest = "?action=" . $_GET["action"];
  if( isset($_GET["id"]) ) {
    $getRequest .= "&id=" . $_GET["id"];
  }
  
  /* formulaire envoyé */
  if ( isset($_POST["subDevoir"]) ) {
    include($profondeur . "assets/php/mysql.php");
    
    /* formulaire d'ajout */
    $infoSupp = "";
    if ( $_GET["action"] == "ajouter" ) {
      $_POST["dateDevoir"] = str_replace("/","-",$_POST["dateDevoir"]);
      $_POST["dateDevoir"] = date("Y-m-d", strtotime($_POST["dateDevoir"]));
      
      if ( !isset($_POST['infoSupp']) ) {
        $infoSupp = "documents et calculatrice non autorises";
      } else {
        foreach ( $_POST['infoSupp'] as $valeur ) {
          $infoSupp .= $valeur . ", " ;
        }
        $infoSupp = substr($infoSupp, 0, -2);
      }
      $query = $connexion -> prepare("INSERT INTO `devoir`(`idUtilisateur`, `dateDevoir`, `infoSupp`, `typeDevoir`, `modalite`, `rattrapage`, `idModule`) VALUES (?, ?, ?, ?, ?, ?, ?)");
      $query -> execute(array($_SESSION["idUtilisateur"], $_POST["dateDevoir"], $infoSupp, $_POST["typeDevoir"], $_POST["modalite"], $_POST["rattrapage"], $_POST["module"]));
      header("Location: ..");
    
    /* formulaire de modification */
    } else if ( $_GET["action"] == "modifier" ) {
      $_POST["dateDevoir"] = str_replace("/","-",$_POST["dateDevoir"]);
      $_POST["dateDevoir"] = date("Y-m-d", strtotime($_POST["dateDevoir"]));
      $query = $connexion -> prepare("UPDATE devoir SET dateDevoir=? WHERE idDevoir=?");
      $query -> execute(array($_POST["dateDevoir"], $_GET["id"]));
      header("Location: ..");
      
    /* formulaire de suppression */    
    } else if ( $_GET["action"] == "supprimer" ) {
      $query = $connexion -> prepare("DELETE FROM devoir WHERE idUtilisateur=? AND idDevoir=?");
      $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
      header("Location: ..");
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php"); ?>
</head>
<body>
  <!-- header & navigation -->
  <?php include($profondeur . "assets/php/navigation.php"); ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
      
        <!-- contenu -->
        <div class="col-md-8">
          
          <form class="form-group formulaire" method="post" action="<?php echo $getRequest; ?>">
          <h1><?php echo ucfirst($_GET["action"]); ?> un devoir</h1>&nbsp;&nbsp;<a href="..">retour</a></br>
            <?php
              include($profondeur . "assets/php/mysql.php");
              if ( $_GET["action"] == "supprimer" || $_GET["action"] == "modifier" ) {
                
                /* verification de l'existence de la donnée */
                $query = $connexion -> prepare("SELECT * FROM devoir WHERE idUtilisateur=? AND idDevoir=?");
                $query -> execute(array($_SESSION["idUtilisateur"], $_GET["id"]));
                $devoir = $query -> fetch(PDO::FETCH_ASSOC);
                $devoir["dateDevoir"] = date("d/m/Y", strtotime($devoir["dateDevoir"]));

                if ( $devoir ) {
                  
                  if ( $_GET["action"] == "supprimer" ) {
                    $query = $connexion -> prepare("SELECT labelModule FROM module WHERE idModule=?");
                    $query -> execute(array($devoir["idModule"]));
                    $module = $query -> fetch(PDO::FETCH_ASSOC);
                    /* formulaire de suppression */
                    echo 'Attention, suppression du devoir ' .$devoir["typeDevoir"]. ' de ' .$module["labelModule"]. ' du ' .$devoir["dateDevoir"]. '!</br>';
                    
                  } else {
                    /* récuperation des labels associés */
                    $query = $connexion -> prepare("SELECT labelModule, labelUE, labelSemestre FROM module, ue, semestre, ueModule, semUE WHERE module.idModule=? AND module.idModule = ueModule.idModule AND ueModule.idUE = ue.idUE AND ue.idUE = semUE.idUE AND semUE.idSemestre = semestre.idSemestre");
                    $query -> execute(array($devoir["idModule"]));
                    $label = $query -> fetch(PDO::FETCH_ASSOC);
                    
                    echo '<label>Semestre </label></br><select type="select"disabled/>';
                    echo '<option>' .ucfirst($label["labelSemestre"]). '</option>';
                    echo '</select></br>';
                    echo '<label>UE</label></br><select type="select"disabled/>';
                    echo '<option>' .ucfirst($label["labelUE"]). '</option>';
                    echo '</select></br>';
                    echo '<label>Module</label></br><select type="select" name="module" disabled/>';
                    echo '<option>' .ucfirst($label["labelModule"]). '</option>';
                    echo '</select></br>';
                    echo '<label>Date</label></br><input type="text" name="dateDevoir" class="datepicker" value="' .$devoir["dateDevoir"]. '" required/></br>';            
                    echo '<label>Supports autorisés</label></br>';
                    echo '<input type="checkbox" disabled ' .((strpos($devoir["infoSupp"],'tous documents autorises')!==false)?'checked':''). '>Tous documents autorisés&nbsp;&nbsp;';
                    echo '<input type="checkbox" disabled ' .((strpos($devoir["infoSupp"],'calculatrice autorisee')!==false)?'checked':''). '>Calculatrice autorisée<br>';
                    echo '<label>Type du devoir: </label></br><select disabled>';
                    echo '<option>' .ucfirst($devoir["typeDevoir"]). '</option>';
                    echo '</select></br>';
                    echo '<label>Modalités du devoir</label></br><select disabled>';
                    echo '<option>' .ucfirst($devoir["modalite"]). '</option>';
                    echo '</select></br>';
                    echo '<label>Devoir de rattrapage ?</label></br>';
                    echo '<input type="radio" disabled ' .(($devoir["rattrapage"]=="oui")?'checked':''). '>Oui&nbsp';
                    echo '<input type="radio" disabled ' .(($devoir["rattrapage"]=="non")?'checked':''). '>Non</br>';
                  }
                  echo '<input type="submit" class="btn btn-default" name="subDevoir" value="' .ucfirst($_GET["action"]). '">';
                } else { header("Location: .."); }

              /* formulaire d'ajout */
              } else if ( $_GET["action"] == "ajouter" ) {
                if ( isset($_POST["subDevoir0"]) ) {
                  $query = $connexion -> prepare("SELECT labelSemestre FROM semestre WHERE idSemestre=?");
                  $query -> execute(array($_POST["semestre"]));
                  $semestre = $query -> fetch(PDO::FETCH_ASSOC);
                  $query = $connexion -> prepare("SELECT ue.idUE, labelUE FROM ue, semUE WHERE semUE.idSemestre=? AND semUE.idUE=ue.idUE");
                  $query -> execute(array($_POST["semestre"]));
                  
                  echo '<label>Semestre </label></br><select type="select"disabled/>';
                  echo '<option>' .ucfirst($semestre["labelSemestre"]). '</option>';
                  echo '</select></br>';
                  echo '<label>UE</label></br><select type="select" name="ue" required/>';
                  while ( $ue = $query -> fetch(PDO::FETCH_ASSOC) ) {
                    echo '<option value="' .$ue["idUE"]. '">' .$ue["labelUE"]. '</option>';
                  }
                  echo '</select></br>';
                  echo '<input type="hidden" name="semestre" value="' .$_POST["semestre"]. '">';
                  echo '<input type="submit" class="btn btn-default" name="subDevoir1" value="Suivant">';
                  
                } else if ( isset($_POST["subDevoir1"]) ) {
                  $query = $connexion -> prepare("SELECT labelSemestre FROM semestre WHERE idSemestre=?");
                  $query -> execute(array($_POST["semestre"]));
                  $semestre = $query -> fetch(PDO::FETCH_ASSOC);
                  $query = $connexion -> prepare("SELECT labelUE FROM ue WHERE idUE=?");
                  $query -> execute(array($_POST["ue"]));
                  $ue = $query -> fetch(PDO::FETCH_ASSOC);
                  $query = $connexion -> prepare("SELECT module.idModule, labelModule FROM module, ueModule WHERE ueModule.idUE=? AND ueModule.idModule=module.idModule");
                  $query -> execute(array($_POST["ue"]));
                  
                  echo '<label>Semestre </label></br><select type="select"disabled/>';
                  echo '<option>' .ucfirst($semestre["labelSemestre"]). '</option>';
                  echo '</select></br>';
                  echo '<label>UE</label></br><select type="select" name="ue" disabled/>';
                  echo '<option>' .ucfirst($ue["labelUE"]). '</option>';
                  echo '</select></br>';
                  echo '<label>Module</label></br><select type="select" name="module" required/>';
                  while ( $module = $query -> fetch(PDO::FETCH_ASSOC) ) {
                    echo '<option value="' .$module["idModule"]. '">' .$module["labelModule"]. '</option>';
                  }
                  echo '</select></br>';
                  echo '<label>Date</label></br><input type="text" name="dateDevoir" class="datepicker" required/></br>';            
                  echo '<label for="infoSupp">Supports autorisés</label></br>';
                  echo '<input type="checkbox" name="infoSupp[]" value="tous documents autorises" >Tous documents autorisés&nbsp;&nbsp;';
                  echo '<input type="checkbox" name="infoSupp[]" value="calculatrice autorisee" >Calculatrice autorisée<br>';            
                  echo '<label for="typeDevoir">Type du devoir</label></br><select name="typeDevoir" required>';
                  echo '<option value="ecrit">Ecrit</option>';
                  echo '<option value="oral">Oral</option>';
                  echo '</select><br>';
                  echo '<label for="modalite">Modalités du devoir</label></br><select name="modalite">';
                  echo '<option value="amphi">Devoir en Amphi</option>';
                  echo '<option value="td">Devoir en TD</option>';
                  echo '<option value="tp">Devoir en TP</option>';
                  echo '</select><br>';
                  echo '<label for="rattrapage">Devoir de rattrapage ?</label></br>';
                  echo '<input type="radio" name="rattrapage" value="oui" required>Oui&nbsp&nbsp';
                  echo '<input type="radio" name="rattrapage" value="non" required>Non<br>';
                  echo '<input type="hidden" name="semestre" value="' .$_POST["semestre"]. '">';
                  echo '<input type="hidden" name="ue" value="' .$_POST["ue"]. '">';
                  echo '<input type="submit" class="btn btn-default" name="subDevoir" value="' .ucfirst($_GET["action"]). '">';
                  
                } else {
                  $query = $connexion -> prepare("SELECT * FROM semestre");
                  $query -> execute();

                  echo '<label>Semestre </label></br><select type="select" name="semestre" required/>';
                  while ( $semestre = $query -> fetch(PDO::FETCH_ASSOC) ) {
                    echo '<option value="' .$semestre["idSemestre"]. '">' .$semestre["labelSemestre"]. '</option>';
                  }
                  echo '</select></br>';
                  echo '<input type="submit" class="btn btn-default" name="subDevoir0" value="Suivant">';
                }
              } else { header("Location: .."); }
            ?> 
          </form>
          <i class="errors"><?php echo $err; ?></i>
        </div>
        
        <!-- alertes -->
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
