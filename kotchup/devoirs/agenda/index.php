<?php
    $profondeur = "../../";
  include($profondeur . "assets/php/hasConnected.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php 
    include($profondeur . "assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <h1>Calendrier des devoirs du mois</h1>
          <?php
            include($profondeur . "assets/php/mysql.php");
            //Creation des variables g�n�rales
            $year = date("Y");
            if ( !isset($_GET['month']) ) {
              $monthnb = date("m");
            } else {
              $monthnb = $_GET['month'];
              $year = $_GET['year'];
              if ( $monthnb <= 0 ) {
                $monthnb = 12;
                $year = $year - 1;
              } elseif ( $monthnb > 12 ) {
                $monthnb = 1;
                $year = $year + 1;
              }
            }
            $day = date("w");
            $nbdays = date("t", mktime(0,0,0,$monthnb,1,$year));
            $firstday = date("w",mktime(0,0,0,$monthnb,1,$year));

            //Tableau pour avoir les jours en 3 lettres
            $daytab[1] = 'Lun';
            $daytab[2] = 'Mar';
            $daytab[3] = 'Mer';
            $daytab[4] = 'Jeu';
            $daytab[5] = 'Ven';
            $daytab[6] = 'Sam';
            $daytab[7] = 'Dim';

            //Cr�ation du tableau calendrier
            $calendar = array();
            $z = (int)$firstday;
            if ( $z == 0 ) $z =7;
            for ( $i = 1; $i <= ($nbdays/5); $i++ ) {
              for ( $j = 1; $j <= 7 && $j-$z+1+(($i*7)-7 ) <= $nbdays; $j++ ) {
                if ( $j < $z && ($j-$z+1+(($i*7)-7)) <= 0 ) {
                  $calendar[$i][$j] = null;
                } else  {
                  $calendar[$i][$j] = $j-$z+1+(($i*7)-7);            
                }
              }
            }
			$mois = array("01"=>"Janvier", "02"=>"Fevrier", "03"=>"Mars", "04"=>"Avril", "05"=>"Mai", "06"=>"Juin", "07"=>"Juillet", "08"=>"Aout", "09"=>"Septembre", "10"=>"Octobre", "11"=>"Novembre", "12"=>"Decembre");
            //Remplace le nombre du mois par son nom
     
            
          ?>
          <h3><?php echo($mois[$monthnb].' '.$year); ?></h3>
          <div class="table-responsive"><table class="resume calendrier table">
          <?php
            echo('<tr>');
            for ( $i = 1; $i <= 7; $i++ ) {
              echo('<th>'.$daytab[$i].'</th>');
            }
            echo('</tr>');
            for($i = 1; $i <= count($calendar); $i++ )  {
              echo('<tr>');
              for( $j = 1; $j <= 7 && $j-$z+1+(($i*7)-7) <= $nbdays; $j++ ) {
                if(strlen($j)==1) {
                  $jour = "0".$j;
                } else {
                  $jour = $j;
                }
                
                $sql = "select * from devoir where dateDevoir='".$year."-".$monthnb."-".$calendar[$i][$j]."' and idUtilisateur=".$_SESSION["idUtilisateur"];
                $requete=$connexion->query($sql);
                while ( $ligne = $requete -> fetch() ) {
                  $query = $connexion -> prepare("SELECT labelModule FROM module WHERE idModule=?");
                  $query -> execute(array($ligne["idModule"]));
                  $module = $query -> fetch();
                  $txt = $txt.'Devoir en '.$module["labelModule"].' - '.$ligne["infoSupp"].'<br>';
                }
                $sql = "select * from devoir where dateDevoir='".$year."-".$monthnb."-".$calendar[$i][$j]."' and idUtilisateur=".$_SESSION["idUtilisateur"];
                $requete=$connexion->query($sql);
                if ( $ligne = $requete -> fetch() ) {
                  echo '<td style="background: #da9091"><a>'.$calendar[$i][$j].'<span>'.$txt.'</span></a></td>';		
                } else  {
                  echo '<td>'.$calendar[$i][$j].'</td>';
                }
                $txt = "";
              }
              echo('</tr>');
            }
          ?>
          </table></div>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
