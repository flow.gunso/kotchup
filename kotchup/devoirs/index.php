﻿<?php
  /* profondeur `à indiquer` afin de gérer les includes en php */
  $profondeur = '../';
  include($profondeur . "assets/php/hasConnected.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include($profondeur . "assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php include($profondeur . "assets/php/navigation.php"); ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php
            include($profondeur . "assets/php/mysql.php");
            $query = $connexion -> prepare("SELECT * FROM devoir WHERE idUtilisateur=? AND dateDevoir > NOW() ORDER BY dateDevoir");
            $query -> execute(array($_SESSION["idUtilisateur"]));
            if ( $query -> rowCount() > 0  ) {
              echo '<h1>Mes devoirs à venir</h1>';
              echo '<div class="table-responsive"><table class="resume table">';
              echo '<tr><th>Date</th><th>Module</th><th>Supports</th><th>Type</th><th>Modalité</th><th>Rattrapage</th><th></th></tr>';
              while ( $devoir = $query -> fetch() ) {
                $query = $connexion -> prepare("SELECT labelModule FROM module WHERE idModule=?");
                $query -> execute(array($devoir["idModule"]));
                $module = $query -> fetch();
                echo '<tr><td>' .date("d/m/Y", strtotime($devoir["dateDevoir"])). '</td><td>' .ucfirst($module["labelModule"]). '</td><td>' .ucfirst($devoir["infoSupp"]). '</td><td>' .ucfirst($devoir["typeDevoir"]). '</td><td>' .ucfirst($devoir["modalite"]). '</td><td>' .ucfirst($devoir["rattrapage"]). '</td><td style="white-space: nowrap;"><a href="changer/?action=modifier&id=' .$devoir["idDevoir"]. '"><span class="glyphicon glyphicon-cog"></span></a><a href="changer/?action=supprimer&id=' .$devoir["idDevoir"]. '"><span class="glyphicon glyphicon-remove"></span></a></td></tr>';
              }
              echo '</table></div>';
            } else { echo "<h1>Aucun devoir prévu à venir.</h1>"; }
          ?>
        </div>
        <?php include($profondeur . "assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>
  
  <!-- footer -->
  <?php include($profondeur . "assets/php/footer.php"); ?>
</body>
</html>
