﻿<?php
  $error = "";
  if ( isset($_POST["subSignUp"]) ) {
    if( $_POST["mdp"] == $_POST["mdpCheck"] ) {
      include("../assets/php/mysql.php");
      $query = $connexion -> prepare("SELECT * FROM utilisateur WHERE pseudo=? OR mail=?");
      $query -> execute(array($_POST["pseudo"], $_POST["mail"]));
      
      
      if ( $query -> rowCount() == 0  ) {
        $mdp = hash('sha512', $_POST["mdp"] . $_POST["pseudo"]);
        $query = $connexion -> prepare("INSERT INTO  utilisateur (pseudo, mp, mail) VALUES (?, ?, ?)");
        $query -> execute(array($_POST["pseudo"], $mdp, $_POST["mail"]));
        
        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-Transfer-Encoding: 8bit";
        $headers[] = "Content-Type: text/html; charset=UTF-8";
        $headers[] = "From: noreply@univ-poitiers.fr";
        
        $message = "Tu viens de t'inscrire sur kotch'up, une application pour t'aider à t'organiser dans ta vie étudiante. Avec cette application, tu vas pouvoir avoir un oeil sur tes absences, tes devoirs à venir et l'avancée de tes candidatures de stages.";
        
        $sujet = "Inscription à kotch'up!";
        mail($_POST["mail"], '=?utf-8?B?'.base64_encode($sujet).'?=', $message, implode("\r\n", $headers));
        
        /* connecte l'utilisateur */
        session_unset();
        session_destroy();
        session_start();
        $query = $connexion -> prepare("SELECT idUtilisateur FROM utilisateur WHERE pseudo=? OR mail=?");
        $query -> execute(array($_POST["pseudo"], $_POST["mail"]));
        $rows = $query -> fetch();
        $_SESSION["pseudo"] = $_POST["pseudo"];
        $_SESSION["idUtilisateur"] = $rows["idUtilisateur"];
        $_SESSION["isConnected"] = true;
        /* debugging disparition idUtilisateur */
        $_SESSION["derniereActivite"] = time();
               
        header("Location: /accueil/");
      } else { $error = "<p>Cet étudiant existe déjà.</p>"; }
    } else { $error = "<p>Les mots de passe ne correspondent pas.</p>"; }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php include("../assets/php/theme.php") ?>
</head>
<body>
  <!-- header & navigation -->
  <?php
    include("../assets/php/navigation.php");
  ?>
  
  <!-- wrapper -->
  <div id="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <form class="form-signin" method="post" action="">
            <h1 class="form-signin-heading">Créez un compte</h1><a href="..">retour</a>
            <label for="pseudo" class="sr-only">Pseudo</label>
            <input type="text" id="pseudo" name="pseudo" class="form-control" placeholder="pseudo" required autofocus>
            <label for="mail" class="sr-only">e-Mail</label>
            <input type="email" id="mail" name="mail" class="form-control" placeholder="e-mail universitaire" required autofocus>
            <label for="mdp" class="sr-only">Mot de passe</label>
            <input type="password" id="mdp" name="mdp" class="form-control" placeholder="mot de passe" required>
            <label for="mdpCheck" class="sr-only">Mot de passe à nouveau</label>
            <input type="password" id="mdpCheck" name="mdpCheck" class="form-control" placeholder="mot de passe à nouveau" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="subSignUp">Créer le compte</button>
          </form>
          <span class="errors"><?php echo $error; ?></span>
        </div>
        <?php include("../assets/php/alertes.php"); ?>
      </div>
    </div>
  </div>

  <!-- footer -->
  <?php include("../assets/php/footer.php"); ?>
</body>
</html>
