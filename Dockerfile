FROM ubuntu:bionic

# Prevent the tzdata package configuration to stop the Dockerfile build.
ENV DEBIAN_FRONTEND=noninteractive

# Install system packages.
RUN apt-get update
RUN apt-get install -y nginx php7.2-fpm php7.2-mysql mysql-server

# Empty the website path beforehand.
RUN rm -rf /var/www/html/

# Push the website, the Docker entrypoint and the Nginx configuration.
COPY kotchup/ /var/www/html/
COPY entrypoint.sh /
COPY nginx.conf /etc/nginx

# Change the website files ownership to the user running Nginx and PHP-FPM.
RUN chown -R www-data.www-data /var/www/html

# Configure the MySQL server. The credentials are here, but meh.
ENV MYSQL_ROOT_PASSWORD=Ieoac83Xne3
ENV MYSQL_USER=kotchup
ENV MYSQL_DATABASE=kotchup
ENV MYSQL_PASSWORD=ixKYOUFiUAc

# Run the equivalent of `mysql secure_installation`
RUN service mysql start ;\
    mysql -u root -e "UPDATE mysql.user SET authentication_string=PASSWORD('$MYSQL_ROOT_PASSWORD') WHERE User='root'; \
        DELETE FROM mysql.user WHERE User=''; \
        DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1'); \
        DROP DATABASE IF EXISTS test; \
        CREATE DATABASE $MYSQL_DATABASE; \
        CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$MYSQL_PASSWORD'; \
        GRANT ALL ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'localhost'; \
        FLUSH PRIVILEGES;" ;\
    service mysql stop

# Enable the PHP MYSQL PDO.
RUN phpenmod pdo_mysql

# Define the MySQL environment variables in PHP-FPM.
RUN printf "env[MYSQL_DATABASE] = $MYSQL_DATABASE\n \
    env[MYSQL_USER] = $MYSQL_USER\n \
    env[MYSQL_PASSWORD] = $MYSQL_PASSWORD" >> /etc/php/7.2/fpm/pool.d/www.conf

# Finally, execute the entrypoint.
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
